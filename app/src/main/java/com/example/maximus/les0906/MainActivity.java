package com.example.maximus.les0906;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    Button write, read;
    EditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        write = (Button) findViewById(R.id.write);
        read = (Button) findViewById(R.id.read);
        editText = (EditText) findViewById(R.id.editText);

        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileOutputStream fos = null;
                try {
                    fos = openFileOutput("file_name", Context.MODE_PRIVATE);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    fos.write(editText.getText().toString().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                editText.setText("");

            }
        });

        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileInputStream fis = null;
                try {
                    fis = openFileInput("file_name");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
                    StringBuilder text = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        text.append(line);
                    }
                    editText.setText(text);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });


//        write.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences sp = getPreferences(Context.MODE_PRIVATE); //open pref file from activity, xml file name == activity name
//                SharedPreferences.Editor editor = sp.edit(); //open editor for edit preferences
//                editor.putString("country", editText.getText().toString());  //put key value pair to pref, BUT not write it to file
//                editor.apply();
//                editText.setText("");
//
//
////                SharedPreferences sp = getSharedPreferences("PrefName", Context.MODE_PRIVATE); //xml file name == “PrefName”
////                SharedPreferences.Editor editor = sp.edit();
////                editor.putString("country", "Ukraine");
////                editor.apply(); //editor.commit();
//
//            }
//        });
//
//        read.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences sp = getPreferences(Context.MODE_PRIVATE);
//                String country = sp.getString("country", editText.getText().toString());
//                editText.setText(country);
//            }
//        });


    }
}
